package demo05;

import figures.Point;

public class PointObservingTest {
	
	public static void main(String[] args) {
		
		Point point1 = new Point(100, 100);
		
		// Di chuyển lần 1
		point1.move(10, 10);
		
		// Di chuyển lần 2
		point1.move(10, 10);
		
		System.out.println("----------------------");
		
		Point point2 = new Point(200, 200);
		
		// Di chuyển lần 1
		point2.move(15, 10);
		
		// Di chuyển lần 2
		point2.move(15, 10);
		
		// Di chuyển lần 3
		point2.move(25, 10);
	}
}
