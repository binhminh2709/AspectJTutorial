package demo05;

import java.util.ArrayList;
import java.util.List;

import figures.Point;

public aspect PointObserving {
	
	//Khai báo kiểu nội bộ (Inter-type declarations)
	//Class Point hoàn toàn không có field: observers
	// Tuy nhiên vẫn có thể khai báo tại đây.
	// observers: Ghi lại các vị trí điểm thay đổi.
	private List<Point>	Point.observers	= new ArrayList<Point>();
	
	pointcut moveAction(Point point) : call(void Point.move(int,int) ) && target(point) && within(PointObservingTest);
	
	after(Point point) : moveAction(point) {
		System.out.println("Point moved");
		// Thêm vào vị trí mới.
		point.observers.add(point);
		
		// Ghi ra các vị trí của điểm đã đi qua.
		System.out.println(" - " + point.observers);
	}
	
	public static void addObserver(Point p) {
		// p.observers.add(s);
	}
	
	public static void removeObserver(Point p) {
		// p.observers.remove(s);
	}
}
