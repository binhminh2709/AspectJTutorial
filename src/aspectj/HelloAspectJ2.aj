package aspectj;

public aspect HelloAspectJ2 {
	
	pointcut callSayHello(): call(* HelloAspectJDemo.sayHello());
	
	// Advice loại "after returning".
	after() returning (Object retObj): callSayHello() {
		System.out.println("Returned normally with " + retObj);
	}
	
	// Advice loại "after throwing".
	after() throwing (Exception e): callSayHello() {
		System.out.println("Threw an exception: " + e);
	}
	
	// Bao gồm cả 2 tình huống trả về bình thường Hoặc bị ngoại lệ khi gọi method (sayHello)
	after() : callSayHello()  {
		System.out.println("Returned or threw an Exception");
	}
	
	//--------------------------------------------------------
	
	//Advice loại "after returning". Quan tâm tới giá trị trả về
	after() returning (Object retObj): callSayHello() {
		System.out.println("Returned normally with Object:" + retObj);
	}
	
	//Advice loại "after returning" không cần quan tâm tới Object trả về.
	after() returning(): callSayHello() {
		System.out.println("Returned normally");
	}
	
	//Hoặc Advice loại "after returning" không cần quan tâm tới Object trả về.
	after() returning: callSayHello() {
		System.out.println("Returned normally");
	}
	
}