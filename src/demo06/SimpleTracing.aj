package demo06;

import org.aspectj.lang.Signature;

public aspect SimpleTracing {
	//Tập hợp các JoinPoint tại vị trí gọi method bất kỳ.
	//và nằm trong class SimpleTracingTest.
	pointcut tracedCall() : call (* *(..))
                 //  && !within(SimpleTracing)
                 && within(SimpleTracingTest);
	
	before() : tracedCall() {
		
		Signature sig = thisJoinPointStaticPart.getSignature();
		String line = "" + thisJoinPointStaticPart.getSourceLocation().getLine();
		
		String sourceName = thisJoinPointStaticPart.getSourceLocation().getWithinType().getCanonicalName();
		//
		System.out.println("Call from " + sourceName + " line " + line + "\n   to " + sig.getDeclaringTypeName() + "." + sig.getName() + "\n");
	}
}
/**
 * Tracking: Nghĩa là theo dấu vết (Các method nào đã được gọi và lần lượt thế nào, hoặc diễn biến của cái gì đó ...)
 * Chúng ta sẽ tạo một Aspect, nó định nghĩa một pointcut trên mọi nơi mà method thực thi,
 * và cũng tạo một Advice chạy đoạn code tại các vị trí đó.
 * 
 * Ví dụ trên tôi giới hạn việc ghi ra thông tin các method được gọi bên trong code của
 * class SimpleTrackingTest. Nếu bạn bỏ điều kiện để loại bỏ các JoinPoint trên chính aspect
 * SimpleTracking, điều này là cần thiết để tránh một vòng lặp vô tận.
 * 
 * Pointcut này chứa tất cả các JoinPoint gọi một method bất kỳ
		tại bất kỳ nơi đâu, ngoại trừ trong SimpleTracking.
		(Để tránh một vòng lặp vô tận).
		pointcut tracedCall() : call (* *(..))
								&& !within(SimpleTracing)
								// && within(SimpleTracingTest)
								;
 * */
