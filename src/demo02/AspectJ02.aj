package demo02;

// Đã import class Point.
import figures.Point;

public aspect AspectJ02 {
	
	pointcut callMove(Point point, int dx, int dy) : call(*  figures.Point.move(int,int)) && args(dx,dy) && target(point) && within(ClassTest02)  ;
	
	before(Point point, int dx, int dy) : callMove(point,  dx, dy )  {
		System.out.println("Before call move(dx,dy)");
	}
}